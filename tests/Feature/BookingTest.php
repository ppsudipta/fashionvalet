<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Controllers\SearchController;

class BookingTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */


        public function testBasicExample()
        {
            // $this->get('/api/search', ['search_string' => 'asdf','filter_by_ride'=>'1'])
            //      ->assertJsonStructure([
            //          'created' => true,
            //      ]);


            //App\Http\Controllers

            //$posts=SearchController::get();



            $posts= $this->get('/api/search', ['search_string' => 'asdf','filter_by_ride'=>'1'])
                 ->assertStatus(200)
                 ->assertJson($posts->toArray())
                 ->assertJsonStructure([
                     '*' => [ 'status', 'message', 'data' ],
                 ]);
        }




}
