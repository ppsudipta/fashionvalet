<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    protected $table = 'booking';


    public function drivers() {
        //  return $this->hasMany( Booking::class );
        return  $this->hasOne(Driver::class, 'id', 'drivers');
        // id--from user

    }
    public function check_status( $param, $id ) {
        $result = Booking::where( 'state', '=', $param )->where( 'drivers', '=', $id )->groupBy('state');
        return $result;
    }

    /**
 * unique_passenger function
 * select number of passenger served by unique driver
 * @param [type] $param state of booking
 * @param [type] $id  id of driver
 * @return void
 */
public function unique_passenger( $param, $id ) {
    $result = Booking::select( 'passenger_id' )->where( 'state', '=', $param )->where( 'drivers', '=', $id )->distinct()->get();

    // dd( $result );
    return $result;
}

  /**
     * total function
     * calculate total of particular one driver
     * @param [type] $id  id of driver
     * @return void
     */
    public function total( $id ) {
        $result = Booking::where( 'state', '=', 'completed' )->where( 'drivers', '=', $id )->sum( 'price' );
        return $result;

    }

}
