<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model {
    //
    protected $table = 'drivers';

    public function bookings() {
        //  return $this->hasMany( Booking::class );
        return  $this->hasMany(Booking::class, 'id', 'drivers');
        // id--from user

    }
/**
 * check_status function
 *
 * @param [type] $param state of booking
 * @param [type] $id id of driver
 * @return void
 */
    public function check_status( $param, $id ) {
        $result = Booking::where( 'state', '=', $param )->where( 'drivers', '=', $id )->groupBy('state');
        return $result;
    }
/**
 * unique_passenger function
 * select number of passenger served by unique driver
 * @param [type] $param state of booking
 * @param [type] $id  id of driver
 * @return void
 */
    public function unique_passenger( $param, $id ) {
        $result = Booking::select( 'passenger_id' )->where( 'state', '=', $param )->where( 'drivers', '=', $id )->distinct()->get();

        // dd( $result );
        return $result;
    }

    /**
     * total function
     * calculate total of particular one driver
     * @param [type] $id  id of driver
     * @return void
     */
    public function total( $id ) {
        $result = Booking::where( 'state', '=', 'completed' )->where( 'drivers', '=', $id )->sum( 'price' );
        return $result;

    }

    public static function searchByKeywords(array $keywords = array(),$filter_by_ride){

        $result = Driver::where(function($query) use ($keywords){
            //  foreach($keywords as $keyword){
            //       $query = $query->orWhere('email', 'LIKE', "%$keyword%");
            //  }

             return $query;
        });

        $result = Booking::select(count('state'))
                        ->where( 'state', '=', 'completed' )
                        ->where( 'drivers', '=', 7)
                        ->havingRaw('count(state) > ?', [$filter_by_ride])
                        ->groupBy('drivers')
                        ->with(['drivers' =>function($query) use($keywords){
                            //  foreach($keywords as $keyword){
                            //       $query = $query->orWhere('email', 'LIKE', "%$keyword%");
                            //  }
                                $query->orWhere('email', 'LIKE', '%fbdrive%');
                        }]);


        return $result->with(['bookings' =>function($result) use($filter_by_ride){
            return $test = $result->select('state')
                        ->where( 'state', '=', 'completed' )
                        ->where( 'drivers', '=', 7)
                        ->havingRaw('count(state) > ?', [$filter_by_ride])
                        ->groupBy('drivers')
                        ->count();
            // if($test > $filter_by_ride){
            //     return $result;
            // }
        }])->get();


        return $result->get()->sortByDesc(function($result) use($filter_by_ride)
            {
                if(!is_null($filter_by_ride)){
                   $return= $result->where()-> check_status('completed',$result->id)->count();

                   if($return>$filter_by_ride){
                       // return $result;
                   }
                   // return $result;


                }

               // return $result->check_status('completed',$result->id)->count();
            });
             // at this line the query will be executed only
                               // after it was built in the last few lines
    }

}
