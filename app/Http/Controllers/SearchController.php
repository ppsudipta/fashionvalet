<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\Http\Controllers\ApiController;
//use DB;


class SearchController extends ApiController {
    //

    /**
    * search function
    *
    * @param Request $request
    * @return void
    */
    public function search( Request $request ) {
        $input = $request->all();
       $search_email = $input['search_string'];

      $filter_by_ride = $input['filter_by_ride'];

        $keywords=explode(",",$search_email);
        $driver_lists=Driver::searchByKeywords($keywords, $filter_by_ride);
        //dd( $driver_lists);
       $process_array=array();
        $i=0;

foreach ($driver_lists as  $driver_list) {

    $process_array[$i]['driver_id'] =$driver_list->drivers;
    $process_array[$i]['number_of_completed_rides'] =$driver_list->check_status('completed',$driver_list->drivers)->count();
    $process_array[$i]['number_of_cancelled_rides'] =$driver_list->check_status('cancelled',$driver_list->drivers)->count();
    $process_array[$i]['number_of_unique_passengers'] =$driver_list->unique_passenger('completed',$driver_list->drivers)->count();
    $process_array[$i]['total_fare'] =$driver_list->total($driver_list->drivers);
    $process_array[$i]['total_commission'] =number_format($driver_list->Total($driver_list->drivers)*0.20, 2, '.', '');
    $i++;

}    return ApiController::successResponse($process_array);
         }

/**
 * addressModify Function
 * address modify  depends on requirement
 * @param Request $request
 * @return void
 */
         public function addressModify( Request $request ) {
            $input = $request->all();
           $input1 = $input['input1'];
           $input2 = $input['input2'];
           $input3 = $input['input3'];
           $process_array=array();

           if(strlen($input1)>30||strlen($input2)>30||strlen($input3)>30)
           {
               $totalstr=$input1.$input2.$input3;
               $positionOfSpace=strrpos(substr($totalstr,0,31)," ");
                $outputline1=trim(substr($totalstr,0,$positionOfSpace));
                $positionOfSpace2=strrpos(substr($totalstr,$positionOfSpace,31)," ");
                $outputline2=trim(substr($totalstr,$positionOfSpace,$positionOfSpace2));
                $outputline3=trim(substr($totalstr,($positionOfSpace+$positionOfSpace2)));
           }
           else{
            $outputline1= $input1;
            $outputline2= $input2;
            $outputline3= $input3;
           }

           $process_array['outputline1']=$outputline1;
           $process_array['outputline2']=$outputline2;
           $process_array['outputline3']=$outputline3;

           return ApiController::successResponse($process_array,"formatted Address Response");

        }


}
