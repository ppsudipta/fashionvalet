<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model {
    //
    protected $table = 'drivers';

    public function bookings() {
        //  return $this->hasMany( Booking::class );
        return  $this->hasMany(Booking::class, 'id', 'drivers');
        // id--from user

    }
/**
 * check_status function
 *
 * @param [type] $param state of booking
 * @param [type] $id id of driver
 * @return void
 */
    public function check_status( $param, $id ) {
        $result = Booking::where( 'state', '=', $param )->where( 'drivers', '=', $id )->groupBy('state');
        return $result;
    }
/**
 * unique_passenger function
 * select number of passenger served by unique driver
 * @param [type] $param state of booking
 * @param [type] $id  id of driver
 * @return void
 */
    public function unique_passenger( $param, $id ) {
        $result = Booking::select( 'passenger_id' )->where( 'state', '=', $param )->where( 'drivers', '=', $id )->distinct()->get();

        // dd( $result );
        return $result;
    }

    /**
     * total function
     * calculate total of particular one driver
     * @param [type] $id  id of driver
     * @return void
     */
    public function total( $id ) {
        $result = Booking::where( 'state', '=', 'completed' )->where( 'drivers', '=', $id )->sum( 'price' );
        return $result;

    }

    public static function searchByKeywords(array $keywords = array(),$filter_by_ride){

        $result = Driver::where(function($query) use ($keywords){
            //  foreach($keywords as $keyword){
            //       $query = $query->orWhere('email', 'LIKE', "%$keyword%");
            //  }

             return $query;
        });

//         $result = DB::select(function($query){
//             $query->select(
//                 DB::raw("count(passenger_id) as total,drivers")
//             )
//             ->from('booking')
//             ->where('state','completed')
//             ->groupBy(['drivers','passenger_id'])
//             ->having('drivers','>',1);
//         })->where('total','>',2);
// dd($result);
        return $result = Booking::select(\DB::raw('count(*) as stateName,  drivers'))//,passenger_id,country_id
                        ->where('state', '=', 'completed' )
                        // ->where( 'drivers', '=', 7)
                        ->having('stateName', '>', $filter_by_ride)
                        //->having('passenger_id', '>', 1)
                        ->groupBy('drivers')


                        ->with(['drivers' =>function($query) use($keywords){
                             foreach($keywords as $keyword){
                                  $query = $query->orWhere('email', 'LIKE', "%$keyword%");
                                }
                            }])
                        ->orderBY('stateName', 'desc')
                        ->get();
 dd($result);


    }

}
